import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(home: new application()));
}

class application extends StatefulWidget {
  @override
  _applicationState createState() => _applicationState();
}

class _applicationState extends State<application> {

  final GlobalKey<ScaffoldState> _skey = new GlobalKey<ScaffoldState> ();

  void method1() {
    _skey.currentState.showSnackBar(new SnackBar(content: new Text('Here is a snackbar')));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _skey,
        body: new Center(
          child: new RaisedButton(
            onPressed: () {method1();},
            child: new Text('SnackBar'),
          ),
        )
    );
  }
}
